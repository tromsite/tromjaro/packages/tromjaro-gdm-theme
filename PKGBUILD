# Maintainer: davedatum <ask at davedatum dot com>

pkgname=tromjaro-gdm-theme
pkgver=20190929
_gnome_shell_ver=1:3.34.0+152+g0fdbde910
_pkgrel=1
_theme=tromjaro-gnome-theme
_hook=gdm-theme-maia
_commit=5834b2fb38744bb22096a052ea8d0ce8df183002
_comm=2dbdf792db061a2935eb8649fddbb27af6a19da6
pkgrel=1
pkgdesc="TROMjaro GDM theme"
arch=(any)
url="https://gitlab.com/tromsite/tromjaro/$_theme"
license=('GPL3')
depends=('gnome-shell')
makedepends=('sassc' 'git')
conflicts=('manjaro-gnome-maia-theme' 'manjaro-gdm-theme')
replaces=('manjaro-gnome-maia-theme' 'manjaro-gdm-theme')
source=("$_theme-$_commit.tar.gz::$url/-/archive/$_commit.tar.gz"
		'gnome-shell-theme.gresource.xml'
		"git+https://gitlab.gnome.org/GNOME/gnome-shell.git#commit=$_comm")
md5sums=('84fee8174c29b910bc97ef4e8374ba29'
         'ac6dd3df728f3fd56eeded20e49ae685'
         'SKIP')

pkgver() {
	date +%Y%m%d
}

prepare() {

#prepare build folder
	if [[ -d build ]]; then
		rm -R build
		mkdir -p build/theme
	else mkdir -p build/theme
	fi

#move the correct files	
	cp -f $srcdir/*.xml	$srcdir/build/theme/
	cp -f $_theme-$_commit/theme/noise-texture.png $srcdir/build/theme/

#generate css files
echo "Generating .css files from gitlab sources..."
workdir=$srcdir/gnome-shell/data
cd $workdir/theme
sed -i '/selected_bg_color: if(\$variant /c\$selected_bg_color: if($variant == 'light', #16a085, #16a085);' gnome-shell-sass/_colors.scss
sassc -a gnome-shell-high-contrast.scss gnome-shell-high-contrast.css
sassc -a gnome-shell.scss gnome-shell.css

# move the css files
echo "replacing the .css files with the new ones..."
cp -f $srcdir/gnome-shell/data/theme/*.css $srcdir/build/theme/
cp -f $srcdir/gnome-shell/data/theme/*.svg $srcdir/build/theme/
#change colors	
cd $srcdir/build/theme

echo "Adjusting svg colors..."
find . -type f -name '*.svg' -exec sed -i \
    "s/#2c1cff/#16a085/Ig;\
    s/#0000ff/#16a085/Ig;\
    s/#0b2e52/#244e37/Ig;\
    s/#1862af/#138971/Ig;\
    s/#3465a4/#3b7666/Ig;\
    s/#006098/#1f7a61/Ig;\
    s/#2b73cc/#17ab8e/Ig;\
    s/#3081e3/#24c891/Ig;\
    s/#15539e/#1b7a61/Ig" {} \;

#compiling schemas
echo "compiling the files"
	cd $srcdir/build/theme 
	glib-compile-resources gnome-shell-theme.gresource.xml

}

package() {

	install -Dm644 $srcdir/build/theme/gnome-shell-theme.gresource $pkgdir/usr/share/themes/manjaro-gdm-themegnome-shell-theme.gresource
	mkdir -p $pkgdir/usr/share/themes
	mkdir -p $pkgdir/usr/share/themes/${pkgname}
	cp -r $srcdir/build/theme/* $pkgdir/usr/share/themes/${pkgname}/
    
    cd $_theme-$_commit
    
    #Hook
    install -Dm644 $srcdir/$_theme-$_commit/$_hook-install.hook $pkgdir/usr/share/libalpm/hooks/$_hook-install.hook
    install -Dm755 $srcdir/$_theme-$_commit/$_hook-install.script $pkgdir/usr/share/libalpm/scripts/$_hook-install
    install -Dm644 $srcdir/$_theme-$_commit/$_hook-remove.hook $pkgdir/usr/share/libalpm/hooks/$_hook-remove.hook
    install -Dm755 $srcdir/$_theme-$_commit/$_hook-remove.script $pkgdir/usr/share/libalpm/scripts/$_hook-remove
    install -Dm644 $srcdir/$_theme-$_commit/gdm-theme-backup.hook $pkgdir/usr/share/libalpm/hooks/gdm-theme-backup.hook
    install -Dm755 $srcdir/$_theme-$_commit/gdm-theme-backup.script $pkgdir/usr/share/libalpm/scripts/gdm-theme-backup

}